# Caer Adalas Campaign

<img src="img/Caer Adalas Impression 02.jpg" alt="Impression of Caer Adalas" title="Caer Adalas" height="50%" width="50%" align="middle">


<!--TOC-->

- [Caer Adalas Campaign](#caer-adalas-campaign)
  - [Introduction](#introduction)
  - [The Setting](#the-setting)
  - [The Characters](#the-characters)
    - [The Player Characters](#the-player-characters)
    - [Non-Player Characters in the Party](#non-player-characters-in-the-party)
  - [The Story So Far](#the-story-so-far)
    - [Session 1 (27-02-2024)](#session-1-27-02-2024)
    - [Session 2 (11-05-2024)](#session-2-11-05-2024)
    - [Session 3 (20-05-2024)](#session-3-20-05-2024)
      - [Entering the Senechor Manor](#entering-the-senechor-manor)
      - [The Basement](#the-basement)
      - [The Secret Room](#the-secret-room)
      - [The Study and the Mercenaries](#the-study-and-the-mercenaries)
      - [The Storage Room and what it had in store](#the-storage-room-and-what-it-had-in-store)
    - [Session 4 (08-06-2024)](#session-4-08-06-2024)
      - [The Ancient Hall](#the-ancient-hall)
      - [Trapped!](#trapped)
      - [To the Mountain Door](#to-the-mountain-door)
      - [Visions](#visions)

<!--TOC-->

## Introduction

This page is a running story update for the Caer Adalas 5th Edition Dungeons & Dragons campaign.

**Fair warning:** this resource is meant for the players in the campaign. My notes can be terse and cryptic and are likely completely uninteresting for the uninitiated.

## The Setting

Caer Adalas is huge and ancient city built atop a high promonotory overlooking a natural harbor that opens out onto an ocean. It is the most prominent city in its part of the world with no real equal. The city is run by an oligarchy that has a representative from each major race. This is ancient tradition but no one remembers where it came from.

The massive natural harbor is the eminent port in the region and there are constantly large merchant vessels arriving and departing.

The city is large with many different sections that cater to most needs, from the ultra-wealthy to the ultra-poor.

## The Characters

### The Player Characters

<img src="img/maruchai.png" alt="Portrait of Maruchai" title="Maruchai" height="10%" width="10%" align="middle">  Maruchai - a Tiefling wizard in search of his past and the truths that are to be gleaned there.

<img src="img/tristan_covari.png" alt="Portrait of Tristan Covari" title="Tristan Covari" height="10%" width="10%" align="middle">  Tristan Covari - A human male of...diverse...talents who enjoys introducing people to the world of the occult.

<img src="img/immianthe_elnala.png" alt="Portrait of Immianthe Elnala" title="Immianthe Elnala" height="10%" width="10%" align="middle">  Immianthe Elnala - A wood elf ranger with unknown origins. She seems to have a strong moral compass. 

<img src="img/kavya.png" alt="Portrait of Kavya" title="Kavya" height="10%" width="10%" align="middle">  Kavya - A mer thief who keeps surprising everyone with new talents they didn't know she had. 

### Non-Player Characters in the Party

<img src="img/davelor_ironflare.png" alt="Portrait of Davelor Ironflare" title="Davelor Ironflare" height="10%" width="10%" align="middle">  Davelor Ironflare - An ex-soldier turned mercenary. He is a man of few words but deep thoughts. He doesn't fight unless he has to but when he is forced to...be somewhere else.

<img src="img/rose_ironflare.png" alt="Portrait of Rose Ironflare" title="Rose Ironflare" height="10%" width="10%" align="middle">  Rose Ironflare - A cleric of Eldath, Goddess of Peace and Life. Her beauty is more than skin-deep and she is often a beacon of hope for those around her. 

- [NPC listing](npcs.md)
- [World Information](world.md)

## The Story So Far

What follow below are summarized notes of what happened during the game sessions so that we can read back the logs and recollect details of our games.



### Session 1 (27-02-2024)

Maruchai, a tiefling, has spent the past few years studying under a master of the Necromantic arts in the upper class part of Caer Adalas. He has been sent out by his mentor to experience the real world instead of spending all this time in bookish contemplation in the school. Being, originally, an out of towner, Maruchai is naive to the city, its customs and its dangers.

While roaming the increasingly more dubious parts of town, a young man presses a flyer into his hand advertising a lecture by "Dr. Ranulph Van Richten" in which the acclaimed doctor indicates he can "Speak with the Dead". This piques Maruchai's interest as he is looking for clues about his mother and his father. Being a tiefling, he has an idea his father may be an extra-planar being but he knows nothing about him. He resolves to attend the seminar.

"Dr. Ranulph Van Richten" (none other than the scammer Tristan Covari) puts on an excellent performance at "The Ruptured Kettle" inn but is eventually tripped up by the arrival of some local goons who, sensing an unauthorized scam proceeding on their turf, demand a cut from Dr. Ranulph. Maruchai, still hoping to get assistance from this speaker with the dead, intervenes and helps Dr. Ranulph to escape both the goons and the city guard that follows hot on their heels.

Dr. Ranulph and Maruchai make their escape together. Ranulph (Tristan) sees that Maruchai has some means and is instantly interested. Dr. Ranulph has already separated Maruchai from some coin for performing his services, and now presses him to pay for their lodgings for the night in a flophouse he knows. While there, Tristan does a tarot card reading for Maruchai.

Hearing of Maruchai's quest for his parents, Tristan decides to assist him and thus helps Maruchai find his way to Althalus's Academy situated between the Waterfront and Carith's Gem (the merchant quarter). There he is informed by the staff that the Loremistress he's looking for is Mistress Jezzara Argaen (Expert on Demonology Lore) but who is unfortunately away on a trip for some days. They agree to paying the downpayment and then to return in a weeks' (10 days') time when the Loremistress should be available again.

In the meantime, they decide to go back to Maruchai's roots, a tiefling village on the far banks of Brigham's Bay. They get a ferry man to transport them across. Arriving there, they try to avoid the Tiefling village in which Maruchai grew up, but then they still run afoul of Barakos, Amelky & Iolix, tiefling rivals from when Maruchai was younger. The tieflings try to waylay and rob Maruchai and Tristan and, in the resulting fight, Barakos is killed and Amelky and Iolix flee. The pair go through Barakos's belongings and recover some money and an obsidian figurine of a demon that Tristan pockets.

From there, they trek inland to find the place where Maruchai's parents lived and where he spent his childhood. It's an abandoned little homestead in a small valley in the mountains. It was clearly a retreat where a mixed couple, human and demon?, could live away from prying, judgemental eyes.

Maruchai pays his respects at his mother's grave and then they search the premises. Finding nothing, at some point, they decide to perform a ritual to see what it might reveal.

An ornate door is revealed in the mountainside behind the house. It is clearly magical in nature and is inset with precious stones (blue, purple, green, yellow and a blood red ruby in the center). An inscription in an unknown language is engraved in the center beneath the ruby.

<img src="img/arcane_door_near_darkhaven.png" alt="Sketch of arcane door" title="Arcane door near Darkhaven" height="300" width="300">

### Session 2 (11-05-2024)

Maruchai and Tristan awaken to a cold morning in the mountains, having camped in the run-down ruins of Maruchai's childhood home. The walls are still sturdy, if crumbling in some places, and the skeleton of a roof in the are they slept in looked sturdy enough to sleep under, keeping the worst of the elements out.

A fresh morning to take a fresh perspective on the mystery of the door they discovered in the mountainside the night before. A brief breakfast and some coffee brewed and they were ready to go at it again.

Firstly, Maruchai prepared his spells for the day and then he cast an incantation that gave him the ability to understand any language he encountered. Instantly, the mysterious inscription on the door was a mystery no longer or, at the very least, the language became apparent. The message itself was cryptic:

"Neither the blood of innocents nor the cries of the world burning move me. Yet sympathy is all I need." 

Tristan summons an invisible servant that he promptly names "Margaret" to help him organize his things and to amuse the time with.

At first they try pressing the colored gems in various sequences to see if this unlocks the door. They try various other methods such as attempting blood magic. Maruchai cuts himself, burns himself and tries to use his blood as a catalyst but this, too, fails. Tristan uses his bardic magic to heal Maruchai of his injuries.

Emotional appeals of pain leave the door literally and figuratively unmoved.

Meanwhile, back in Darkhaven, the Mournclaw Mercenary company has taken over the town for a few days. Their reasons for being there are unclear, even to most of the hired sell-swords. Per Mournclaw and his lieutenants have been keeping their cards close to their chests. What the regular mercs are doing is bullying the population into staying in their homes, imposing a "curfew" to keep them in line.

Immianthe is called up by Roth Vursk, one of Per's lieutenats, together with two other mercs to clear out one of the fancy houses on the town circle adjacent to the large, "Founding Family's" house at the top of the circle (against the mountain). When the 3 of them arrive at the front door and knock on it, a fearful Tiefling face greets them, cracking the door open only slightly.

When instructed to leave their home and move to one of the other houses on the circle, they beg and plead for mercy. Immianthe, Mia, is inclined to let them stay but then Roth arrives and asks what the hold-up is. Mia's explanation falls on deaf ears and he orders them to get the family out of the house. Sighing, Mia instructs the family that they must leave...they then plead for some leniency and time to get their things together.

Mia allows them to take some more time but then more mercenaries arrive, carrying what look like rope and tackle and what appear to be mining tools. They try to shoulder past Mia and find her blocking the way. Growling for her to move and charging at her aggressively, the mercenary, who had a length of rope circling around his burly shoulder finds him self being pulle down to Mia's height, the rope twisted around his throat in a tight lock that pulls him off-balance and to the side, his air supply cut off.

The mercs reach for their weapons but at that moment one of the other mercs that had been assigned to clear the house with Mia intervenes. His sword is somehow already in hand and the sharp edge is resting at one of the mercenaries' throats. While they stand at detente, Mia asks the 3rd person in their group, the lady, to go inside and assist the family to evacuate the house.

Mia tells the mercenary she has in a vice grip that she'll let him go if he behaves and she gets a grudging grunt of confirmation from him. Back at a detente, the tension high, the 2 groups square off. The lady returns however and informs them that the house is clear and that they can proceed.

Disgruntled 3 of the mercs enter the house but not before ordering the other 3 to take the group into custody for reporting to a lieutenant for disciplining. As they're marched away, flanked by the mercenaries, Mia and the others introduce themselves to each other.

The soldier is called Davelor Ironflare and the lady is his sister, Rose Ironflare. They were hard up for money and the "adventuring" company that offered board, food and pay looked attractive. They hadn't signed up for evicting civilians out of their homes and whatever it is that's going on in DarkHaven.

Tristan and Maruchai, meanwhile, eventually give up on trying to open the door, realizing that if there is a secret to it, they are no closer to unlocking it than when they started. Resigned, they pack their things and leave the little valley in which the house is located. As they approach the exit, they look back and note to their astonishment that the door has once more disappeared, hidden by whatever magics guard it.

As they exit through the narrow crevass that's the only path in and out of the valley (besides the mysterious door) they encounter Maravari Moonbridge. She is an old acquaintance (adolescent crush) of Maruchai's and she seems relieved to see him. She says that she'd come up here looking for him as she'd heard he was about from Amelky.

They briefly catch up on what's happened in their lives: Maravari has been working with her parents in the family trading business and has become quite the adept merchant.

Maravari brings Maruchai and Tristan up to speed on the situation unfolding in DarkHaven and says that she fears the worst for her parents and family.

Maruchai and Tristan agree to help her and they proceed to the outskirts of DarkHaven to scout out the situation. Maravari mentions that she'd escaped the town by means of a secret route that they often used as young tieflings to get away.

DarkHaven sits between a natural harbor on Brigham's Bay and some mountains. In the harbor are fishing boats that belong to the townspeople and, currently, a large longboat with a sail that clearly belongs to the mercenaries. At the "top" of the town, nearest the mountains is a large semi-circle that contains a fountain. At the 'center' of the semi-circle is a large house belonging to the family that founded the town. Around the curve of the semi-circle looking towards the fountain are large 2 and 3 story stone buildings belonging to the wealthy families of the town including the Moonbridge residence. Fanning out from the circle are avenues that lead downhill through the town, one of which runs straight down to the harbor. Most of the houses are made out of stone and wood.

The town looks largely deserted, with only sporadic patrols moving about. Most of the activity, according to Maravari, is up in the town circle. The mercenaries seem to be up to something in the largest house of the "Founding Family" that sits at the base of the mountain overlooking the rest of the town down the incline. It is a large structure with a first floor balcony overlooking the town circle where the family could address an audience if it wished. Nobody seems clear on what precisely is happening in there.

Maravari tells Tristan and Maruchai that the mercenaries have imposed a curfew and enforce that people stay in their own homes "for their own safety". Besides patrols, they see a few townspeople moving around with carts and other logistical things required to keep the occupation running (cleaning chamberpots and transporting sewage, bringing fresh food up to the invaders, etc.).

Having scoped out the town from up a hill, they make their way down and into the town. While the mercenaries have put up some makeshift fortifications around the town (simple wooden barricades to slow down outside attackers), they do not seem to have the manpower (or interest) to man the barricades continually. It's easy enough to enter unnoticed.

Weaving through the smaller streets of the town, they notice a shutter quickly being closed when they pass by, showing some activity inside the house. Maravari goes to the door and knocks, eventually getting someone to answer the door (which they do furtively). She is able to agree with them that they would open the door for her and her companions if they needed a place to hide out.

Continuing to the Moonbridge residence, they sneak in through the lush back garden. It's a 2-story structure and, as they approach the back door, they listen for any sounds. They hear the sounds of muffled protests and struggling from one of the rooms directly above the back door.

Tristan, Maruchai and Maravari sneak in through the back door and look around, moving through a hallway they reach the center of the house which has a central courtyard with internal galleries running around the perimeter of it on both floors. Maravari shows them the stairs up and they arrive near the room.

Hearing the sounds of what is clearly a lady in distress, Tristan barges into the room, rapier drawn to see the  horrible sight of a mercenary pinning Maravari's mother to the bed and taking advantage of her.

There are 2 other mercenaries in the room in a state of half-undress clearly awaiting "their turn". Maruchai joins the fight as the mercenaries, surprised by the new arrivals, slowly turn to face them.

Meanwhile, Mia, Rose and Davelor are on the town circle, being led past the Moonbridge residence. The sound of fighting reaches them and the mercenaries escorting them hurry towards the house to find out what's going on. Th 3 follow.

Arriving near the bedroom where the fight with the mercenaries has drawn to a conclusion, with Maruchai and Tristan victorious, the lead mercenary slows down and keeps some distance from the entrance, not wanting to be ambushed. He calls out for his colleagues or whoever is in the room to show themselves. The silence he is met with his not encouraging.

Understanding the situation, Mia and Davelor jump into action and take out 2 of the mercenaries. The 3rd bolts and escapes.

In the room, Tristan and Maruchai have taken up tactical positions behind the furniture and are facing the door, indeed awaiting anyone foolish enough to enter. Maravari and her mother are hidden under the bed where Maravari is doing her best to sooth her traumatized mother.

Mia makes a careful entry, telling them she's coming in with her hands up and that she's not going to attack.

After some careful back and forth, the group introduces themselves to each other briefly, understanding they are not enemies and Rose, indicating she's a healer, tends to Maravari's mother.

### Session 3 (20-05-2024)

After taking a few moments to collect themselves, the group take stock of their situation. One of the mercenaries got away and they realize that they may not have that much time until reinforcements come looking for them.

Rose has calmed and healed Mrs. Moonbridge sufficiently to get some more words out of her. She and Maravari discover that Maravari's dad was taken away but not necessarily killed in front of Mrs. Moonbridge. It may be that he is still alive.

The group leave the premises and make their way back to the house where the occupants had promised to allow them entry if they needed it.

They crowd into a small but cosy home which belongs to a blacksmith's family. Adjacent to the house is the smithy in which Maruchai sets up and busies himself casting a `Find Familiar` spell to summon a bat as his familiar.

Tristan, meanwhile, does some readings of the cards and some fun coin tricks for the family members to help them relax. Rose tends to their health and heals them where they require it. Mia takes up a position on the top floor, looking out of a window to watch the approaches to the house to make sure they're not taken by surprise. Davelor seems to relax with the family but seems on alert in case he's needed.

Maruchai uses his bat to scout out the town, seeing that there aren't more than about 4 pairs of mercenaries patrolling the streets, a skeleton crew, really. Most of the town's inhabitants are holed up, unharmed, in their homes. Shifting focus to the large house at the top of the Town Circle, the bat flies in through the balcony door and gets a sense of there being a lot of activity there. He is chased out by a servant wielding a broom.

Fluttering about the outside of the large house, the bat picks up snippets of sounds and complaints. There are the sounds of people trudging about the property, and sounds of things being ripped off walls and destroyed. Finally, the bat picks up the sound of a mercenary complaining about the fact that "they hadn't found that bloody secret door yet, and that it didn't seem like they were going to." After it is done scouting, it returns to the house the group are staying at, to nestle under the eaves of the roof.

Maruchai relates what he's discovered to the group and they formulate a plan. They need to investigate the manor but they need to get rid of as many of the mercenaries in the house to do so. They formulate a plan to set the boat in the harbor alight, in order to draw out some of the mercenaries from the house.

It's decided that Mia and Davelor will set the ship alight and then rejoin the group at the manor. They set out and manage to land 8 fire arrows into the ship before high-tailing it towards the manor uptown.

#### Entering the Senechor Manor

In the meantime, having the bat scout ahead of them to reduce the risk of encountering patrols, Maruchai, Tristan and Rose make their way up to the town circle to one of the houses most adjacent to the large Manor (the Senechor Family manor) and scout it out for means of entry.

The guards at the front door were doubled, seemingly after discovering the bodies of the mercenaries at the Moonbridge Estate. There are now 4 guards standing there.

As the 3 observe, a hue and cry is raised about the longboat burning in the harbor and a squad of 15 mercenaries is sent running towards it, carrying buckets with them.

They scale over the garden wall and drop into the fancy back garden, hiding in the hedges. Avoiding a guard taking a much needed leak against a fancy tree, they find an entry point through the side door that's there for the kitchen staff.

As they enter and explore the kitchen, Davelor and Mia arrive at the outer wall and Maruchai's bat squeaks at them from the place the first group scaled the wall. They quickly scale over the wall at the same place and find the same entry door, joining the rest of the group.

Inside the house, they quickly strategize about which way is best to go. They quickly settle on taking the spiral staircase down into what must be the basement level, assuming that there will be a lower chance of being spotted there.

#### The Basement

<img src="img/DarkHaven Senechor Manor basement.png" alt="Map of Senechor Manor - Basement" title="Senechor Manor - Basement">

They arrive in the pantry which clearly supplies the kitchen. Listening at one set of doors, they hear there is a large chamber on the other side and there is the sound of mining and activity. They decide to take the other door.

> Marching Order: Davelor, Rose, Tristan, Maruchai and Mia

Davelor leads them as quietly as possible down the corridor, they check some of the doors to the small rooms on the side but these turn out to be rooms where a lot of junk has been deposited and forgotten.

Peeking around a bend, he sees some mercenaries walking in and out of the large chamber's other entrance. He can't make out what they're carrying from there but he informs the others.

They try to determine how to proceed and Tristan indicates he can throw up a simple illusion at the end of the hall showing an "empty hall" to the mercenaries so that the party can sneak up closer without being seen.

This tactic works and gets them up close to the chamber entrance undetected. They see one mercenary walk out carrying a box full of what appears to be rubble.

Peeking around the corner to look into the chamber, they see that the chamber is pitted with holes dug by the mercenaries and there are 2 actively being dug at the moment. The holes are so deep that the mercenaries digging them can no longer be seen.

There are another 2 mercenaries at the far wall (towards the back of the house) where they appear to be digging a long corridor.

As they watch, another mercenary arrives (turning right in front of them to enter the chamber and not noticing the group due to the illusion). This mercenary seems to be bringing tidings of how the fight to douse the longboat fire is going.

Tristan casts another `Minor Illusion` but this time on the entry way to the large chamber, allowing the group to slip past. They continue down the hall towards the spiral stairs when Maruchai hears sounds coming from one of the small rooms. Listening at the door, Maruchai has a strong suspicion these are the prisoners, the heads of the merchant houses that run the town, who were rounded up by the mercenaries for questioning.

They try the door, but it is locked. No one in the group knows how to pick a lock and so the only option is to open the door with crowbars. However, Davelor points out that the noise would bring the mercenaries ontop of them and so it would be wiser to get the jump on them first before trying the door.

The group agrees and they return to the large chamber, peeking around the door to see the situation. After a quick and decisive battle, they kill all the mercenaries with no real damage to themselves. Just as they're about to take the next step another mercenary arrives, carrying some beers for the group. He's stunned to see the scenen in the room. An arrow from Mia in his side convinces him to surrender and he allows the party to tie him up and question him.

He gives them some information about what's going on: Per Mournclaw, the leader of the Mournclaw Mercenary Company, is searching for some sort of artifact that he believes is hidden away in a secret chamber. Despite having found various secret hidey-holes and even a secret chamber (that apparently contained family genealogy information), none of them contained the desired item and Per is convinced there is another secret chamber.

When asked where they can find the secret room they found, he tells them it's in the study on the 1st floor.

They stuff the sock of one of his dead colleagues in his mouth to prevent the mercenary from sounding the alarm. Tristan searches the mercenaries for gold and keys and finds a key ring with keys and approximately 8 Crowns (GP).

Heading back out to the hallway, they find the appropriate key for the locked door (one of the rooms opposite the wine cellar) and open it, finding, as they expected, the hostages taken by the mercenary company. One of the hostages is Arteros Senechor, the head of the Senechor family whose manor they're in. Another is Astoradon Moonbridge, Maravari's father. 

They explain they were questioned and that Mournclaw was interested in the whereabouts of some secret chamber and an artifact is apparently a wrist-band with a purple gem inset into it. They didn't know anything about these things and at some point he tired of them and had them locked away down here.

The group indicates they're going to continue exploring the mansion but that the heads of the families can make a safe exit by means of the kitchen entrance the group came in by. Astoradon Moonblade thanks them profusely for rescuing them, but Arteros Senechor insists on joining them as this is "his mansion and he will not run from its occupiers".

#### The Secret Room

The party, accompanied by Arteros Senechor, take the nearby spiral staircase back upstairs. They make for the ground floor Dining room where the mercenary indicated they'd found a secret room. 

<img src="img/DarkHaven Senechor Manor Ground.png" alt="Map of Senechor Manor - Ground Floor" title="Senechor Manor - Ground Floor">

They don't encounter any resistance on their way to the Dining room and they enter through the doorway closest to the pspiral stairs (just down a corridor). The dining room is huge and lavish as one might expect of such a wealthy household but where once there was a magnificent fireplace, there's now a gaping hole in the wall where the mercenaries broke through it to find the secret room.

The room itself is richly dressed with a soft, springy carpet, wood-paneled walls with expensive (if now rotting) silk wall coverings and a large lectern stand towards the center back of the room.

On each of the wall panels of the room hangs a portrait of what appear to be family members of the Senechor line. However, none of the names are familiar to Arteros!

Maruchai goes up onto the dais and tries to read the book. It appears to be a genealogy book of the Senechor family, but an ancient branch of it. It is written in an ancient dialect of Abyssal. With a little effort he is able to read it. On the first page is the inscription:

>"On this day of Our Lord, Archaeus, we of the House Senechor, Guardians of the Stone and Gate, have claimed this land as our own and will hold it at the price of our lives and our honor. Woe to the enemies of the Adalaens and a blight upon She Who Rules Beyond the Sea. We record here the story of our clan that our duty and our charge will not be forgotten." - Tiraeus Senechor

In the meantime, Tristan has his Unseen Servant "Margaret" tap on the walls and explore, hoping to find a secret cubby. To his surprise, this actually bears fruit when a hexagonally shaped part of the wall opens up under one of the portraits, revealing a hidden scroll.

Maruchai reads the scroll and discovers it is a `Speak with Dead` scroll. How fortunate! He hangs on to it.

When questioned about this room and the people on the walls, Arteros confesses to being as puzzled as they are. He does not recognize any of the family, although he recognizes some of the names on the wall as names that have been in the family. He has had uncles and aunts with those names and he speculates that these must be ancient ancestors that have now been forgotten. He is equally mystified as to the purpose of the room and why it has been hidden away.

Eventually, Maruchai notices one of the portraits, labelled "Kara Senechor", and he's shocked by how much it likeness resembles that of his mother's. When he brings this up, Arteros is, at first evasive, but finally admits that Maruchai's mother was indeed a member of the Senechor family. The family has long had a mixed lineage of Tiefling and Human presenting members. Maruchai's mother, Amelia, fell in a love with a stranger, a visiting Tiefling, who had wild ideas about secrets being hidden in the Senechor mansion. He claimed the mansion was much older than the people in town seemed to think and he was convinced there was much more to Darkhaven than they gave it credit for.

The family forbade Amelia from marrying this strange pauper, but she went against their will and married him. Divested of her wealth and stature, Amelia and Maruchai's father settled in the cottage they built in the valley some distance away from Darkhaven. Clearly, however, given the strange, magical door is there in the mountain, they probably had an ulterior motive for choosing that spot!

Having seen enough in the room, they exit it only to have Tristan notice one more feature of the house. The far wall (towards the back of the manor) of the dining hall is strangely built up against the mountain. On the one hand, this provides a natural barrier from anyone entering the manor and its grounds from that direction, on the other, it *is* a strange construction. He asks Maruchai to check for enchantments in the direction of that wall.

Maruchai picks up a faint magical essence coming from upstairs.

#### The Study and the Mercenaries

They quickly go back to the spiral staircase and move one floor up, entering the study.

<img src="img/DarkHaven Senechor Manor 01.png" alt="Map of Senechor Manor - First Floor" title="Senechor Manor - First Floor">

Here, they encounter the infamous Per Mournclaw and 2 of his lieutenants, one of them being Roth, the lieutenant that had given the initial orders to Mia, Davelor and Rose. They round on the intruders, but Per holds up a hand and asks them what they're doing here.

They enter into a negotiation, Per explains he's looking for a bracelet with a purple stone set into it and that this is what is employer is paying heavily for. He doesn't divulge who the employer is. After some discussion, they agree to work together temporarily to help Per find his hidden room. He seems satisfied to allow them to share in the spoils as long as the bracelet is given to him.

The magical aura is stronger here, but Maruchai indicates it is still a floor higher and so they, and the mercenaries, move upstairs to the top floor.

#### The Storage Room and what it had in store

<img src="img/DarkHaven Senechor Manor 02.png" alt="Map of Senechor Manor - Second Floor" title="Senechor Manor - Second Floor">

Moving up the spiral stairs again, the enter the storage room and here, when Maruchai enters the room, a door appears in the outer wall that's pressed up against the mountain face (on the far side of the corridor door through which they enter). This door too bears and inscription in abyssal:

> "Do not call our Lord's name in vain".

They quickly realize the answer to this riddle and Maruchai enjoins the door to open in the name of "Archaeus" (the lord described in the genealogy book).

The door opens to a mysterious darkness inside.

### Session 4 (08-06-2024)

#### The Ancient Hall

Kavya has just finished helping the Mournclaw Mercenaries with dousing the fire on their longboat. Since there's nothing more to be done there, the mercenaries leave four men to guard the boat and order Kavya to join them to the manor.

Arriving there, they go in search of Per Mournclaw to inform him about the status of the longboat fire and that it is under control.

They finally find Per in the 3rd floor storage room where he has just discovered the secret doorway along with the PCs.

Seeing the Mer (Kavya) in their number he realizes he has some disposable trap fodder to send through the ancient doorway in order to test for traps.

Kavya is ordered through and enters the doorway, finding herself on a landing at the top of a stone spiral staircase.

As she moves down the stairway she feels the third step sink and click and she realizes a trap has been triggered. Luckily, her reflexes save her and she is able to dive backwards into a backflip to avoid the 3 darts that cut across the step from the outside wall and embedding themselves into the inner stone core around which the staircase turns.

Davelor offers to take point and uses his quarterstaff to probe the next few steps, slowly making his way down, and warning those following him about which steps to avoid.

Just before reaching the bottom, Davelor has an uncertain moment, the very last step on the stairs clicks as if it were a trapped step, but no darts seem to come out. What the step does seems to be a mystery and he finally decides to step over it and into the chamber beyond. 

<img src="img/Darkhaven Seneschor Manor Ancient Chamber.png" alt="Drawing of the Ancient Hall underneath Senechor Manor" title="Senechor Manor - Ancient Hall">

The rest of the party follows them in with Per and one of his lieutenants bringing up the rear. The chamber is very old, they can tell from the dust and the stale air. No one has been here in quite some time. From where they stand at the apex, the room forms a pentagon with the far side of the room forming the base of the pentagon.

Each of the 5 walls has a door set into with an intricate locking mechanism built into each one. The floor of the main chamber has a soft incline towards the center, the purpose of which is unclear.

Each wall around the door has an exquisite frieze carved into it. From left of the stairs and going around in a clockwise direction:
   - A man sits on an impressive throne, a crown (gold plating) on his head set with a blue sapphire in the center. He looks grim and around him are arrayed impressive looking forces of equally grim looking soldiers armed to the teeth. In a layer beneath this image, the king is shown ordering an army of men into war.
   - The unknown creature towers over her people, pointing them onto ships and boats. There skin is covered with what appear to be golden veins. There is something dark and evil about these creatures, the way they are depicted casts something ominous about them.
   - The Tiefling wizard wears a gauntlet with a purple stone set into it weaving powerful, dark magics, skeletons rising from the battelfield.
   - The dwarven king wears a breastplate with a yellow stone in it. He is directing his dwarves to fight and build fortifications. The structure they're building looks suspiciously like a proto-Caer Adalas.
   - An ethereally beautiful carving of an Elven forest, an elven queen wearing a necklace inset with a green stone. She is surrounded by elven rangers, archers and wizards and charging through a forest into a battlefield.

The group splits up to each examine a different wall/door. Rose and Davelor examine the elven door, Per and his lieutenant go look at the Dwarven door, Kavya and Maruchai go to the human door, Mia to the unknown creatures' door and Tristan to the Tiefling door.

Noting the lock on the door, Tristan asks Kavya to join him and asks if she's able to open that lock. Examining it, she sets to work. Her skill with lockpicks is good and she quickly determines that the locks are cleverly designed. A less skilled lockpicker might easily open the door using an obvious mechanism but this would appear to prime a trap of some kind. Probing a little further, she determines there is a deeper, harder to find mechanism that unlocks the door safely. She uses this mechanism and the complex mechanism on the surface of the door starts to move and turn and then door unbolts and smoothly swings open into the chamber beyond.

Davelor moves over to where they are and enters the room to investigate. Per takes an interest, but from a distance. From what Tristan and Kavya kan see through the opened door is that the room is fairly large but quite barren. To the left and right there seem to be some stalagmites rising out of the ground but if those have somehow formed over the centuries or where part of the original design of the room is impossible to tell. Directly opposite the door, on the far side of the room, stands a dais with on top of it a polished, dark wood box (small chest).

Tristan uses his psionic ability to lift up the lid of the box. Immediately, the door slams shut in their faces, trapping Davelor inside as they hear the roar of some enraged beast!

Kavya sets to unlocking the door again and when she gets it open, Davelor is taking cover behind some stalagmites to the left of the door while a large, humanoid, white-furred creature (a Quaggoth Thonot) seems to have appeared from somewhere and is moving menacingly towards him.

Rose and Mia cut into the room and head for the stalagmites near Davelor to support him while Tristan hurls some `Vicious Mockery` at the beast who seems susceptible to it. A psionic push has no effect on it however.

While it's distracted, Mia plants an arrow in its skull causing it some real pain. Deciding that things are getting too hot for it, the Quaggoth casts `Mirror Self` and duplicates itself twice so that there are now three of them.

The battle continues but it is rather one-sided, between the 5 adventurers and the creature. They quickly dispatch it, leaving them to explore the room.

Finally reaching the box, Mia finds an ornate bracer in it with a large purple gemstone set into it.

<img src="img/Tiefling Purple Stone Bracer.jpg" alt="Rendered image of bracer with purple gemstone" title="Purple Gemstone Bracer" height="40%" width="40%" align="middle" />

She pockets it for now and doesn't mention it to the others.

In the meantime, Tristan and Kavya try some of the other doors. Each room is decorated in accordance with the aesthetic of the race it 'represents'. The Elven room is white marble with stonework that looks almost like it was grown rather than hewed. The Dwarven chamber is flawlessly built and looks as if new even after these many centuries, etc. In order of their searching they encounter the following.

The 'Dwarven' door: this time Kavya opens the door and Tristan uses his telekinetic ability to open the chest inside from a distance. As usual, the door slams shut again (this time trapping no one inside). There is an earth-shattering slamming sound and the floor vibrates, dust shook loose from the ceiling. Then a heavy mechanism is hear, like a pully and chains as something heavy is lifted again. When Kavya reopens the door, everything in the room, except the box and pedestal has been crushed to fine dust. It appears the ceiling drops down as a trap when someone tries to open the box. Sadly, the box appears to be empty.

They apply the same tactic to the 'Elven' room. In this case, when they re-open the door after it has slammed shut upon telekinetically opening the chest, they find that the room is filled with crawling venomous insects like centipedes and scorpions, etc. They manage to slam the door shut again after ascertaining from a distance that the box is empty.

While they're busy dealing with all of the rooms, one of Per's lieutenants heads up the stairs again and Per hangs out near the base of the stairs, watching them but not lifting a finger to help.

Maruchai and Kavya open the next room, the human one. Here, opening the box creates a magical field of darkness throughout the room, making it impossible to see. Luckily, Maruchai's familiar is a bat and he can view through its echolocation senses! He scouts out the room and sees that the box is also empty. It also appears as if some nasty spike traps have been activated on the floor of the room. Fortunately, there is no need to traverse it so they close the door and Maruchai summons his familiar back to his side. 

Finally Kavya opens the door to the 'unknown' race's room. Here, when the chest is opened from a distance, the door slams shut and the sound of water crashing into the room is heard. Opening the door again a large volume of, what is apparently salt, water spills out, moving down the slightly inclined pentagonal floor of the main chamber down to a drain in the center. Here, too, the chest is frustratingly empty.

Having noted that all the rooms' chests were empty (no one knows of the bracer Mia found as yet), Per himself retreats back up to the main house frustrated at the result but not wishing to let these troublseome adventurers (and deserters from his crew: Mia, Davelor and Rose) leave knowing the secrets of the house.

The group follow them carefully up, picking past the stairs traps themselves but find on the landing where the secret entrance was that they came through that it has been closed by Per Mournclaw, effectively trapping them inside!
 
#### Trapped!

Per is convinced that the party is hiding something from him, perhaps they found the bracer and are not revealing it to them.

Tristan manages to convince Per to let Maruchai and himself out by tempting Per with information about the magical door in the mountain they discovered.

Since Per wants to keep some collateral and doesn't want to let the deserters from his crew escape, he agrees to allow Tristan and Maruchai out so that they can show him the door, but he insists on leaving the rest locked inside the ancient hall and stairway.

Tristan and Maruchai are treated to a lavish dinner while the rest of the party remains trapped miserably on the landing!

Still Maruchai sends his familiar to keep an eye on the hallway outside the secret door, to see if he can give any assistance to his friends should they manage to get outStill Maruchai sends his familiar to keep an eye on the hallway outside the secret door, to see if he can give any assistance to his friends should they manage to get out.

Mia in the meantime decides on a plan of action. She's not sure how to get the door open but she knows that there is a semi-circle of mercenaries on the other side with cross-bows, hiding behind cover and just waiting for them to try to make their esape.

She uses `Fog Cloud` to create a dense fog in the room outside the door, making it impossible for the crossbow men to see.

Maruchai sees through his familiar's eyes that something is happening, so he calls out, through his familiar, the secret word that opens the door: "Archaeus!"

The party finds the door opening precisely as they would've hoped and quickly file out into the fog, avoiding the mercenaries and making their way back to the spiral staircase through which they originally made their way up.

Returning back to the basement level, they are able to backtrack their route of entry to make their escape.

Maruchai and Davelor, in the meantime, enjoy a hearty dinner and are given a room to spend the night in (under watch, of course).

#### To the Mountain Door

In the morning the Per Mournclaw, a contingent of his mercenaries and Tristan and Maruchai set out for the Mountain Door.

Following them at a safe distance are Mia, Davelor, Rose and Kavya.

They arrive in the valley quite quickly, as it is not far from town if you know where to look.

The mountain door is, of course, magically concealed and Tristan and Maruchai have no idea how to reveal it.

Per grows increasingly frustrated with the group and Tristran tells him that door is definitely there.

In frustration, the mercenaries pound and chip at the wall with some pickaxes and discover, to their surprise, that these tools have no effect on a particular part of the mountain wall and the tools even chip when hit against that area forcefully.

After this, Per is convinced there is something there but, given they cannot reveal it, he decides to leave.

The party settles in for a bit, catching up with each other and discussing various options.

Maruchai decides to use the `Speak with Dead` scroll Tristan uncovered from the hidden family room at the Senechor Manor to speak with his deceased mother.

They carefully exhume her body (which is still a lightly fleshy skeleton) and read from the scroll to cast the spell.

His mother is not as conversant as he recalls, and her responses are brief and often times enigmatic. Maruchai asks the following questions and receives the answers indicated:
 
   - Where is father? *Dead*
   - How do I open the door? *Don't know*
   - What's behind the door? *Hope*
   - What do you know about the door? *Servants*
   - Where are the other hidden locales? *In Darkhaven.*

The answers are not quite what they would have hoped.

After they are done, Mia reveals to the group the bracer she discovered in the ancient hall and, when Maruchai puts it on, it magically fits itself to his wrist and seems to 'knit' closed around his arm with no obvious way of removing it!

They note the similarity of the purple stone to the one on the magical door and decide to explore this further.

#### Visions

Moving to where the magical door in the mountain is, Maruchai holds out the bracer on his arm and the door appears.

Moving the bracer over the surface, he feels an attraction to the purple gem set into the door but as he moves it over the other gems there seems to be no response.

Finally, he moves the bracer to the purple gem and touches gem to gem and his mind is flooded with visions:

   - In a room whose white walls seem woven from the trunks of slender trees, with almost magical rays of sunlight breaking through to add warmth and light to the space, the most beautiful being he's ever seen, an elvish princess, looks up. She has violet eyes and silver hair. She is shocked at first, then slow realization dawns in her ethereal eyes, as her hand rises to a green stone on a necklace around her neck. She is filled with sadness but doesn't know why.
   - In a deep, huge hall of stone, far underground and lit by the heat of molten stone and ancient magics, a dwarf looks up from his toil, sweat on his broad brow. He, too, is stunned, his hand going to his chest where, in the most intricately made breastplate he has ever seen, sits a yellow stone. As realization dawns on the dwarf as to what is happening, his eyes widen and he feels a powerful desire rising in him, a need so fundamental to his being that his lip curls into a snarl.
   - Somewhere, in a location that seems completely dark and devoid of any light except for some mysterious light specks floating around, truly, the most beautiful being he's ever seen reclines. They are more beautiful even than the Elven princess he saw but moments ago. They have dark skin with veins of gold forming an interconnected, root-like pattern over their body. Their eyes are blue, their hair white, and their feminine-presenting beauty makes the watcher's heart ache. Their eyes flick open and the most powerful, burning, white hot desire flows through Maruchai. On their forehead they wear a simple circlet with a V dip at the front, into which is set a large red stone. Their eyes shift and they seem to look straight at Maruchai and fear lances through him.
   - In a distant kingdom, in a bedroom lit by strange, magic lights, an old man lying in a bed snaps his eyes open. His face bears the lines of a life spent shouldering heavy responsibility and he looks tired. His eyes grow wider as he sees something, horror growing in his gaze. The blue stone set into his crown glints as the man goes into a spasming fit and dies of fear.
  
 Meanwhile, not to far away from Maruchai, Kavya is changing. Her deep blue skin seems to become less dull and a richer hue of dark blue. Golden vein patterns form over her skin. She looks a lot like that creature that Maruchai just saw.
 
 Kavya herself feels more awake, more alive, more **her**. Thoughts come more quickly, senses are sharper, as if she's awakening from a long half-slumber.
 
 Maruchai relates to the party what he has seen. 
 
 Maruchai's familiar, who has been circling the area, reports that, while, Per Mournclaw's men left with him, 2 sentries were left behind on the slopes flanking the crevice in and out of the valley. He informs Maruchai that one of them is leaving to follow after Mournclaw. Probably they have witnessed the appearance of the door (and maybe even the bracer) and run off to inform their boss of these new developments.
 
 The remaining sentry is still watching them and, after Maruchai explains this to the party, Kavya decides to sneak up above the sentry and is able get the jump on him and knock him out. Giving them some privacy.
 
 The rest of the party busy themselves setting up a camp so that they are ready for the evening as it is moving towards late afternoon and it's probably too late to set out on a journey.
 
 However, as they near completion of the camp setup, Maruchai's familiar informs them that he has seen a large company of men approaching the other side of the crevice.
 
 Mournclaw is back, and this time he's brought a large contingent of his mercenaries.
 
 ### Session 5 (06-07-2024)
 
 #### The Battle for Darkhaven Dale
 
 Maruchai hurries to the entrance of the crevice through which the Mournclaw Company is filing, 2 abreast. His bat has revealed that the company is 22 strong including Per Mournclaw himself. As the first mercenaries become visible, Maruchai casts `Web` and manages to trap the first 4 mercenaries where they stand, blocking the company from entering further while the party take potshots at the trapped soldiers.
 
Mia is able to shoot at them from the relative safety of the first floor bedroom of the dilapadated house. Davelor joins Maruchai near the entrance to the dale at the crevice to help fight and pick off any mercenaries that make it through.

Kavya takes up a position on the slopes, not too far from the crevice, where she'd found a sentry earlier. Rose and Tristan and rose join Mia up in the 1st floor bedroom, having imparted various inspiration and/or blessings on those doing the fighting.

Despite the odds being against them, the clever use of the terrain makes the battle decidedly one-sided in the party's favor. Mia and Davelor continue to pepper the hapless mercenaries with crossbow bolts and arrows. Maruchai launches various spells at them like `shatter` and `chill touch` to dog the people still stuck in the defile. The echoing `shatter` spell is almost like a detonation in that small space and kills several mercenaries outright.

Soon, some of the mercenaries get the idea of climbing up the slopes and coming over parts of he hills. The first group of 3 find Kavya and there begins a pitched fight that almost ends her!

Maruchai, meanwhile, tries to help Kavya with the mercenaries besetting her but unfortunately misses his `Chill Touch` attack and accidentally hits Mia instead!

Fortunately, aside from her own skills, Kavya has Mia's bow to help her and between the two of them, they manage to defeat the mercenaries on the slope.

A wizard has apparently been brought in by Per to assist him but the man is also stuck with the other mercenaries. The `Shatter` spell has also injured him some and in frustration he casts `Melf's Acid Arrow` at Maruchai who is mostly protected from the spell due to Tristan's "friendship" (a protective spell lending him temporary hitpoints).

Davelor fires a crossbow bolt into the wizard and then Rose uses her `Command` spell to make the man flee from them.

He cuts through the other mercenaries, creating even more chaos in the ranks.

In the meantime, 3 more mercenaries come over the slope on the other side of the crevice, trying to get the jump on Davelor. However, Maruchai has already spotted them coming with his familiar and so he warns Davelor. As the mercernaries become visible, Davelor fires a crossbow bolt into one. Maruchai makes another attempt at a `Chill Touch` attack and this time hits Davelor! (He's having a bad run of luck!)

With the help of Mia and a `Guiding Bolt` from Rose, Davelor is able to dispatch the 3 mercenaries. By this point, Maruchai can see by way of his familiar that a heated discussion has broken out within the 7 remaining mercenaries on the other side of the crevice. Clearly, Per is no longer able to bolster their morale and loses the vote on whether or not to continue. They turn back towards Darkhaven.

The band briefly celebrates their victory, by picking through the pockets of the fallen mercenaries to enrich themselves with. On average, each of them scored 20 Crowns in total. They bed down for the night in the ruins of the house, Davelor taking first watch and Mia relieving him when she's had her 4 hours' meditation rest.

The night is uneventful and the next day they return to Darkhaven, only to find that the mercenary longboat has left the town, returning towards Caer Adalas.

They manage to charter two skiffs to take them over the Bay back to Caer Adalas and, upon arriving there, see that the longboat has docked and, besides a few token guards, is now empty of mercenaries.

Asking around the Morass (the less savory part of Caer Adalas where they've arrived), they find out that Per Mournclaw and company have set off towards the Spine (a more well-to-do part of town).

Kavya stops a Mer she knows who gives her a partly awed, and partly fearful, look as she sees the golden veins running along her skin. The Mer informs her that other "Touched" Mer have appeared with these veins and are asking questions and "starting trouble" throughout the city.

The group also sees signs of this, more open clashes with Mer asking for what they deserve. Mer protesting in front of a warehouse, asking for fair pay, etc.

Making their way up the Spine, they need to ask around again to see if anyone saw a party matching Per and company's description. Eventually, they meet an enterprising young fella named Bjorn (In response to the question why he does what he does, he says: "My mum pooped me out and named me Bjorn." And then shrugs). He informs them, after some silver crosses his palm, that a group matching their description turned down Farcrow Avenue.

The group hire in Bjorn and his associates to help them scour the area for more intelligence. Soon, word comes back that the group turned off Farcrow to head up a road to a private estate behind an ornate gate.

Arriving there, the group finds they can just open the gate and walk in, but the house guards refuse them entrance or any real information about the occupants.

Eventually, they learn through questioning a non-touched Mer gardener that the house and estate belongs to Assistant Archon Arturus Seneschus and his wife Illia Seneschus. He is a well-placed politician in the government of Caer Adalas and wields quite some clout.

Having found out what they could, the group decide to return to the place some of them were recruited into the Mournclaw Mercenary company (back down in the Morass), in the hopes of finding out more about whoever originally awarded them the contract.

Here, too, unfortunately, they draw a blank, most of the ads that are posted up on the boards are the old ones that Mia, Davelor and Rose responded to some days earlier.  
