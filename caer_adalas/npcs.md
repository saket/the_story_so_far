# Non-Player Characters (NPCs)

## Caer Adalas

| Name | Affiliation | Status | Details |
|------|:-----------:|:------:|---------|
| Mistress Jezzara Argaen | Althalus Academy || Expert on Demonology Lore at the Althalus Academy. Tristan and Maruchai have an appointment with them when they return from their research trip |
| Pielkee | Althalus Academy || Halfling; Receptionist/Greeter at the Althalus Academy |

## DarkHaven 

| Name                      |             Affiliation             |   Status   | Details                                                                                                                                                                                                                        |   |   |
|---------------------------|:-----------------------------------:|:----------:|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|---|
| Barakos                   |           DarkHaven Thug            | `Deceased` | Tiefling that had beef with Maruchai and tried to rob Maruchai and Tristan                                                                                                                                                     |   |   |
| Amelky                    |           DarkHaven Thug            |            | Female Tiefling accomplice of Barakos                                                                                                                                                                                          |   |   |
| Iolix                     |           DarkHaven Thug            |            | Tiefling accomplice of Barakos                                                                                                                                                                                                 |   |   |
| Maravari Moonbridge       |         Darkhaven Merchant          |            | Tiefling; Maruchai's adolescent crush. Beautiful and capable lady from one of the wealthy merchant families in Darkhaven. She comes soliciting the PCs' help when DarkHaven is occupied by the Per Mournclaw's mercenary band. |   |   |
| Per Mournclaw             |        Mournclaw Mercenaries        |            | Leader of the mercenaries invading Darkhaven                                                                                                                                                                                   |   |   |
| Roth Vursk                |        Mournclaw Mercenaries        |            | One of Per's lieutenants who ordered Immianthe, Rose and Davelor about                                                                                                                                                         |   |   |
| Tenesa Moonbridge         |          Maravari's Mother          |            | Female Tiefling who was being ravaged by some of Mournclaw's mercenaries                                                                                                                                                       |   |   |
| Astoradon Moonbridge      |          Maravari's Father          |            | Tiefling head of the Moonbridge family and a wealthy merchant. He was captured, questioned and imprisoned by the Mournclaw Mercenaries                                                                                         |   |   |
| Arteros Senechor          | Current head of the Senechor Family |            | Tiefling head of the Senechor family and Maruchai's distant uncle who turned away Maruchai's father when he came to take Maruchai's mother's hand in marriage                                                                  |   |   |
| [Amelia](Amelia) Senechor | Maruchai's human-presenting Mother  |            | Maruchai's mother who passed away when he was young and took his father's secrets to her grave. Maruchai wishes to speak with her in order to learn more about who he is.                                                      |   |   |
