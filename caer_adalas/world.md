# Caer Adalas Setting Information

## Caer Adalas and environs
### Calendar/Time

#### Adaelaen Era

**Current Year:** 4105 Post Foundation (PF)

##### Historic Milestones


##### Months of the Year


##### Days of the Week


##### Hours of the Day


### Pantheon

Each race has its own pantheon bu the most popular gods are known to the other races as they are referenced in a lot of speech.

Many gods are more local but through their representatives their reach can be far afield

#### The Common Pantheon of Caer Adelas

|               Holy Symbol                | Divinity Name | Alignment | Divinity of                                              | Pronouns  | Domains                     |
|:----------------------------------------:|---------------|:---------:|----------------------------------------------------------|-----------|-----------------------------|
|              A Spiked Lock               | Glyph/Glif    |    CN     | Goddess of Hidden things                                 | She/Her   | Knowledge, Trickery         |
|         A Winding, Silvery Road          | Vala          |    LN     | Goddess of Paths & Ways                                  | She/Her   | Knowledge, Nature & Light   |
|               A Pentagram                | Tekra         |    LG     | Divinity of Protection                                   | They/Them | Light, Life and War         |
|             An Eagle's Head              | Adelos        |    LG     | Eagle God, Divinity of Seeing, Patron God of Caer Adelas | He/Him    | Nature, Knowledge and Light |
|             A budding plant              | Balor         |    NG     | Divinity of children, offspring                          | They/Them | Life, Light and Death       |
|                Gold coins                | Bezos         |    NE     | God of greed and shiny things                            |           | Trickery and Death          |
|      Three Pillars depicting a ruin      | Ameera        |    CE     | Divinity of Destruction                                  | They/Them | War, Death and Tempest      |
|   Two crossed swords pointing downward   | Féor          |    TN     | Goddess of Conflict                                      | She/Her   | War and Death               |
|          A swirling water flow           | Havar         |    NG     | God of Water, Weather & Cleansing                        | He/Him    | Nature and Tempest          |
| A skull set against a backround radiance | Grim          |    LG     | God of the Dead                                          | He/Him    | Death, War and Knowledge    |
|                 The Sun                  | Sarav         |    LE     | Goddess of Power                                         | She/Her   | War, Nature and Knowledge   |

#### Gods by Peoples


|  Holy Symbol   | Divinity Name | Alignment | Divinity of                                                | Pronouns  | Domains                   |
|:--------------:|---------------|:---------:|------------------------------------------------------------|-----------|---------------------------|
| A Broken Heart | Adhuri        |     N     | Mer Divinity of Separation, Division and unfinished things | They/Them | Life, Trickery, Knowledge |

### Locations

#### Adalan

**Capital:** Caer Adalas
**Population:**

##### Caer Adalas

##### Currency

  - Fleet (Platinum) = 10 Crowns
  - Crown (Gold) = 10 Boats
  - Boat (Silver) = 10 Skiffs
  - Skiff (Copper)

##### Government

##### Districts

- The Eyrie (Keep where the ruling class lives)
- The Nest (Outer ring within the keep walls, where the Nobles and Wealthy live)
- The Spine (Winding road down the mountain-side to the lower districts)
- Carith's Gem (Mercantile district behind the Waterfront (between mountain and waterfront))
- Waterfront (harbors ports)
  - College of Lore (Althalus's Academy):
- The Morass (along Brigham's Bay) - the poor and desperate part of the Caer upto the Saar Delta)
   - The Ruptured Kettle Inn (Tristan's show)
   - Taran's Tavern (Kavya stole food from here when she was younger)


##### DarkHaven

Situated across Brigham's Bay from Caer Adalas, DarkHaven is a small town founded by tieflings and humans trying to build a life together away from the judgemental eyes of Caer Adalas's society. Nestled between the mountains and a natural harbor on the bay, the people of DarkHaven mine and trade ore from the mountains for goods from Caer Adalas. They also have a vibrant fishing community which provides the bulk of their food.
